import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:mobile_core_app/core/utils/get_form_error_text.dart';
import 'package:provider/provider.dart';
import '../../../core/models/auth/login_model.dart';
import '../../../core/providers/auth_provider.dart';
import '../../../core/utils/alert.dart';
import '../../../core/utils/constants.dart';
import '../../../core/utils/custom_form_build_style.dart';
import '../../../core/utils/custom_login_input.dart';
import '../../../core/utils/string_converter.dart';

class LoginForm extends StatelessWidget {
  const LoginForm({
    Key key,
    @required GlobalKey<FormBuilderState> fbKey,
    @required ThemeData theme,
  })  : _fbKey = fbKey,
        _theme = theme,
        super(key: key);

  final GlobalKey<FormBuilderState> _fbKey;
  final ThemeData _theme;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20.0),
      child: FormBuilder(
        key: _fbKey,
        child: Column(
          children: <Widget>[
            Container(
              decoration: kBoxDecorationStyle(
                color: CommonColors.coreLight,
              ),
              alignment: Alignment.centerLeft,
              height: 60,
              child: FormBuilderTextField(
                name: 'username',
                cursorColor: _theme.iconTheme.color,
                style: TextStyle(
                  fontFamily: CommonFonts.body,
                  color: _theme.iconTheme.color,
                ),
                decoration: fbLoginTextFieldStyle(
                    labelText: "Username",
                    icon: CommonIcons.user,
                    theme: _theme),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(context),
                ]),
              ),
            ),
            SizedBox(height: 20),
            Container(
              decoration: kBoxDecorationStyle(
                color: CommonColors.coreLight,
              ),
              alignment: Alignment.centerLeft,
              height: 60,
              child: Theme(
                data: _theme.copyWith(
                  primaryColor: _theme.iconTheme.color,
                ),
                child: Consumer<AuthProvider>(
                  builder: (_, state, child) => FormBuilderTextField(
                    name: 'password',
                    obscureText: state.obsurceText,
                    cursorColor: _theme.iconTheme.color,
                    style: TextStyle(
                      fontFamily: CommonFonts.body,
                      color: _theme.iconTheme.color,
                    ),
                    keyboardType: TextInputType.visiblePassword,
                    decoration: fbLoginTextFieldStyle(
                      labelText: "Password",
                      icon: CommonIcons.password,
                      theme: _theme,
                      suffixIsExist: true,
                      suffixIcon: (state.obsurceText)
                          ? CommonIcons.hide
                          : CommonIcons.show,
                      onPressedSuffixIcon: () =>
                          context.read<AuthProvider>().toggleSuffixIcon(),
                    ),
                    validator: FormBuilderValidators.compose([
                      FormBuilderValidators.required(context),
                    ]),
                  ),
                ),
              ),
            ),
            SizedBox(height: 20),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                TextButton(
                    onPressed: () {}, child: Text('Don\'t have an account ?')),
                TextButton(onPressed: () {}, child: Text('Forgot ?'))
              ],
            ),
            Container(
              margin: EdgeInsets.only(top: 40.0, bottom: 20.0),
              width: double.infinity,
              height: 60.0,
              child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    textStyle: TextStyle(color: Colors.white),
                    padding: EdgeInsets.all(20.0),
                    elevation: 0.0,
                    shadowColor: Colors.transparent,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                  ),
                  onPressed: () {
                    if (_fbKey.currentState.saveAndValidate()) {
                      final _formDoc =
                          LogInModel.fromJson(_fbKey.currentState.value);
                      Provider.of<AuthProvider>(context, listen: false)
                          .logIn(formDoc: _formDoc, context: context);
                    } else {
                      String _errorText =
                          getFormErrorText(formState: _fbKey.currentState)
                              .toLowerCase()
                              .capitalize();

                      Alert().show(
                          message: _errorText,
                          alertIcon: CommonIcons.alertError,
                          alertBackGroundColor: CommonColors.danger,
                          context: context);
                    }
                  },
                  child: Text(
                    'LOGIN',
                    style: TextStyle(
                      color: CommonColors.core,
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                      fontFamily: CommonFonts.header,
                    ),
                  )),
            ),
          ],
        ),
      ),
    );
  }
}
