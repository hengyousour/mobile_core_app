import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import '../../../core/widgets/responsive.dart';
import '../../../core/storages/connection_storage.dart';
import '../../../core/utils/constants.dart';
import '../../../app.dart';

class ConnectionForm extends StatelessWidget {
  const ConnectionForm({
    Key key,
    @required ThemeData theme,
  })  : _theme = theme,
        super(key: key);

  final ThemeData _theme;

  @override
  Widget build(BuildContext context) {
    final MediaQueryData _mediaQuery = MediaQuery.of(context);
    double _buttonWidth = 0.15;
    if (Responsive.isMobile(context)) _buttonWidth = 0.20;
    return FadeIn(
      duration: Duration(milliseconds: 1800),
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          children: <Widget>[
            Material(
              color: Colors.transparent,
              child: IconButton(
                splashRadius: 35.0,
                icon: Icon(CommonIcons.noConnection),
                splashColor: CommonColors.core.withOpacity(0.1),
                onPressed: () => _settingModalBottomSheet(context),
                iconSize: 60,
                color: CommonColors.core,
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            SpinKitThreeBounce(
              color: _theme.colorScheme.background,
              size: 20.0,
            ),
            SizedBox(
              height: 10.0,
            ),
            Text(
              'Oops, Can\'t Connect To Server',
              style: TextStyle(
                  color: _theme.colorScheme.background,
                  fontSize: 24.0,
                  fontFamily: CommonFonts.header,
                  fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 10.0,
            ),
            Text(
              'Please Check Your Internet Connection Or IP Address And Try Again',
              style: TextStyle(
                color: _theme.colorScheme.background,
                fontFamily: CommonFonts.body,
              ),
            ),
            SizedBox(
              height: 60.0,
            ),
            Container(
              width: _mediaQuery.size.width * _buttonWidth,
              decoration: BoxDecoration(
                  color: _theme.colorScheme.background,
                  borderRadius: BorderRadius.circular(30.0)),
              child: TextButton(
                onPressed: () {
                  meteor.reconnect();
                },
                style: TextButton.styleFrom(
                  padding: EdgeInsets.all(20.0),
                ),
                child: Text(
                  'Retry',
                  style: TextStyle(
                    color: _theme.primaryColor,
                    fontWeight: FontWeight.bold,
                    fontFamily: CommonFonts.header,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

void _settingModalBottomSheet(
  BuildContext context,
) {
  final ThemeData _theme = Theme.of(context);
  final _connectionStorage = ConnectionStorage();

  showModalBottomSheet(
      context: context,
      builder: (BuildContext bc) {
        return Container(
          child: Wrap(
            children: <Widget>[
              ListTile(
                leading: Icon(CommonIcons.ipAddress),
                title: Text(
                  'IP Address',
                  style: TextStyle(fontFamily: CommonFonts.body),
                ),
                onTap: () async {
                  Navigator.pop(context);
                  showDialog(
                      context: context,
                      builder: (_) {
                        return Responsive(
                            mobile: SettingForm(
                                theme: _theme,
                                connectionStorage: _connectionStorage),
                            tablet: Row(
                              children: [
                                Spacer(),
                                Expanded(
                                  flex: 4,
                                  child: SettingForm(
                                      theme: _theme,
                                      connectionStorage: _connectionStorage),
                                ),
                                Spacer(),
                              ],
                            ),
                            desktop: Row(
                              children: [
                                Spacer(),
                                Expanded(
                                  child: SettingForm(
                                      theme: _theme,
                                      connectionStorage: _connectionStorage),
                                ),
                                Spacer()
                              ],
                            ));
                      });
                },
              ),
              ListTile(
                leading: Icon(CommonIcons.qrCode),
                title: Text(
                  'QR Code',
                  style: TextStyle(fontFamily: CommonFonts.body),
                ),
                onTap: () async {
                  Navigator.pop(context);
                  FlutterBarcodeScanner.scanBarcode(
                          "#000000", "Cancel", true, ScanMode.DEFAULT)
                      .then((result) async {
                    if (result != '-1') {
                      _connectionStorage.setIpAddress(ip: result);
                      Phoenix.rebirth(context);
                    }
                  }).catchError((err) => {print(err)});
                },
              ),
            ],
          ),
        );
      });
}

class SettingForm extends StatelessWidget {
  const SettingForm({
    Key key,
    @required ThemeData theme,
    @required ConnectionStorage connectionStorage,
  })  : _theme = theme,
        _connectionStorage = connectionStorage,
        super(key: key);

  static final GlobalKey<FormBuilderState> _fbKey =
      GlobalKey<FormBuilderState>();
  final ThemeData _theme;
  final ConnectionStorage _connectionStorage;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(
        'Setting',
        textAlign: TextAlign.center,
        style: TextStyle(
            color: _theme.primaryColor,
            fontSize: 28.0,
            fontWeight: FontWeight.bold,
            fontFamily: CommonFonts.header),
      ),
      content: FormBuilder(
        key: _fbKey,
        child: FormBuilderTextField(
          name: "ipAddress",
          cursorColor: _theme.primaryColor,
          style: TextStyle(
            fontFamily: CommonFonts.body,
          ),
          decoration: InputDecoration(
            errorStyle: TextStyle(
              fontFamily: CommonFonts.body,
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(width: 3.0, color: CommonColors.primary),
            ),
            contentPadding: EdgeInsets.only(top: 14),
            prefixIcon:
                Icon(CommonIcons.configIpAddress, color: _theme.primaryColor),
            hintText: 'IP Address',
            hintStyle: TextStyle(
              fontFamily: CommonFonts.body,
            ),
          ),
          validator: FormBuilderValidators.compose([
            FormBuilderValidators.required(context),
          ]),
        ),
      ),
      actions: <Widget>[
        Container(
          width: MediaQuery.of(context).size.width * 1,
          decoration: BoxDecoration(
            color: _theme.primaryColor,
            borderRadius: BorderRadius.circular(5.0),
          ),
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(
              textStyle: TextStyle(color: Colors.white),
              padding: EdgeInsets.all(20.0),
              elevation: 0.0,
              shadowColor: Colors.transparent,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
            ),
            onPressed: () async {
              if (_fbKey.currentState.saveAndValidate()) {
                _connectionStorage.setIpAddress(
                    ip: _fbKey.currentState.value['ipAddress']);
                Navigator.pop(context);
                Phoenix.rebirth(context);
              }
            },
            child: Text(
              'Set Up',
              style: TextStyle(
                fontSize: 20,
                fontFamily: CommonFonts.body,
                color: _theme.colorScheme.background,
              ),
            ),
          ),
        )
      ],
    );
  }
}
