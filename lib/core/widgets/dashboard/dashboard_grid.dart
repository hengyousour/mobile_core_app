import 'package:flutter/material.dart';
import 'package:mobile_core_app/core/services/dashboard_services.dart';
import '../../../core/models/dashboard/dashboard_model.dart';
import '../responsive.dart';
import 'dashboard_grid_list.dart';

class DashBoardGrid extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final List<DashboardModel> _dashboardList =
        DashboardServices(context: context).dashboardList;
    int _crossAxisCount;
    return OrientationBuilder(
      builder: (context, orientation) {
        if (orientation == Orientation.portrait) {
          if (Responsive.isDesktop(context) | Responsive.isTablet(context)) {
            _crossAxisCount = 4;
          } else {
            _crossAxisCount = 2;
          }
        } else {
          _crossAxisCount = 4;
        }
        return GridView.builder(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: _crossAxisCount,
            childAspectRatio: 1,
            crossAxisSpacing: 20,
            mainAxisSpacing: 20,
          ),
          itemCount: _dashboardList.length,
          itemBuilder: (ctx, i) {
            return DashBoardGridList(
              dashboard: _dashboardList[i],
            );
          },
        );
      },
    );
  }
}
