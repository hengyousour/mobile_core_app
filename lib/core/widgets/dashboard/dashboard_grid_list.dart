import 'package:flutter/material.dart';
import 'dart:math' show pi;
import 'package:mobile_core_app/core/models/dashboard/dashboard_model.dart';
import 'package:mobile_core_app/core/utils/constants.dart';

class DashBoardGridList extends StatelessWidget {
  const DashBoardGridList({
    Key key,
    @required DashboardModel dashboard,
  })  : _dashboard = dashboard,
        super(key: key);
  final DashboardModel _dashboard;

  @override
  Widget build(BuildContext context) {
    final ThemeData _theme = Theme.of(context);
    return Container(
      decoration: BoxDecoration(
          color: _theme.primaryColor,
          boxShadow: CommonBoxShadow().defualt,
          borderRadius: BorderRadius.circular(10.0)),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          padding: EdgeInsets.all(0.0),
          elevation: 0.0,
          shadowColor: Colors.transparent,
        ),
        onPressed: () => _dashboard.onPressed(context),
        child: Stack(
          children: [
            Positioned(
              top: 25,
              left: -29,
              child: ClipRRect(
                child: Container(
                  height: _dashboard.iconSize,
                  child: Transform.rotate(
                    angle: 32 * pi / 180.0,
                    child: Icon(
                      _dashboard.iconName,
                      size: _dashboard.iconSize,
                      color: _theme.colorScheme.background.withOpacity(0.1),
                    ),
                  ),
                ),
              ),
            ),
            Center(
              child: Text(
                _dashboard.title.toUpperCase(),
                style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 25,
                    color: _theme.colorScheme.background,
                    fontFamily: CommonFonts.body),
              ),
            )
          ],
        ),
      ),
    );
  }
}
