import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import '../utils/constants.dart';

class WaitingData extends StatelessWidget {
  final Color textColor;
  final Color loadingColor;
  const WaitingData(
      {Key key, @required this.textColor, @required this.loadingColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        SizedBox(
          height: 10.0,
        ),
        Text(
          'Waiting',
          style: TextStyle(
              fontFamily: CommonFonts.body, fontSize: 20.0, color: textColor),
        ),
        SizedBox(
          height: 10.0,
        ),
        SpinKitThreeBounce(
          color: loadingColor,
          size: 20.0,
        ),
      ],
    );
  }
}
