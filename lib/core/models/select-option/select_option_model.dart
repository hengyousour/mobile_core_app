import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'select_option_model.g.dart';

@JsonSerializable(createToJson: false)
class SelectOptionModel {
  final String label;
  final dynamic value;

  const SelectOptionModel({
    @required this.label,
    @required this.value,
  });

  factory SelectOptionModel.fromJson(Map<String, dynamic> data) =>
      _$SelectOptionModelFromJson(data);
}
