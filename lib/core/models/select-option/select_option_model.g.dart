// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'select_option_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SelectOptionModel _$SelectOptionModelFromJson(Map<String, dynamic> json) {
  return SelectOptionModel(
    label: json['label'] as String,
    value: json['value'],
  );
}
