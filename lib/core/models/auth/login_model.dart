import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'login_model.g.dart';

@JsonSerializable()
class LogInModel {
  final String username;
  final String password;

  const LogInModel({@required this.username, @required this.password});
  factory LogInModel.fromJson(Map<String, dynamic> data) => _$LogInModelFromJson(data);
  Map<String, dynamic> toJson() => _$LogInModelToJson(this);
}
 
      