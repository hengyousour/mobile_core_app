import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'setting_model.g.dart';

@JsonSerializable(createToJson: false)
class SettingModel {
  final String ipAddress;
  final String language;
  const SettingModel({@required this.ipAddress, @required this.language});

  factory SettingModel.fromJson(Map<String, dynamic> data) =>
      _$SettingModelFromJson(data);
}
