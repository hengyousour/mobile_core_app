// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'setting_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SettingModel _$SettingModelFromJson(Map<String, dynamic> json) {
  return SettingModel(
    ipAddress: json['ipAddress'] as String,
    language: json['language'] as String,
  );
}
