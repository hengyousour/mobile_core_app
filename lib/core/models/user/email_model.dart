import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'email_model.g.dart';

@JsonSerializable()
class EmailModel {
  final String address;
  final String verified;
  const EmailModel({@required this.address, @required this.verified});
  factory EmailModel.fromJson(Map<String, dynamic> data) =>
      _$EmailModelFromJson(data);
  Map<String, dynamic> toJson() => _$EmailModelToJson(this);
}
