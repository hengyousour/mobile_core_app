import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';
import './email_model.dart';

part 'user_model.g.dart';

@JsonSerializable(explicitToJson: true)
class UserModel {
  final String id;
  final String username;
  final List<EmailModel> emails;
  const UserModel(
      {@required this.id, @required this.username, @required this.emails});
  factory UserModel.fromJson(Map<String, dynamic> data) =>
      _$UserModelFromJson(data);
  Map<String, dynamic> toJson() => _$UserModelToJson(this);
}
