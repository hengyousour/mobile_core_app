// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'email_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EmailModel _$EmailModelFromJson(Map<String, dynamic> json) {
  return EmailModel(
    address: json['address'] as String,
    verified: json['verified'] as String,
  );
}

Map<String, dynamic> _$EmailModelToJson(EmailModel instance) =>
    <String, dynamic>{
      'address': instance.address,
      'verified': instance.verified,
    };
