import 'package:flutter/material.dart';
import '../utils/constants.dart';

final kHintTextStyle = TextStyle(
  color: Colors.white54,
  fontFamily: CommonFonts.body,
);

final kLabelStyle = TextStyle(
  color: CommonColors.core,
  fontWeight: FontWeight.bold,
  fontFamily: CommonFonts.body,
);

BoxDecoration kBoxDecorationStyle({@required Color color}) {
  return BoxDecoration(
    color: color,
    borderRadius: BorderRadius.circular(5.0),
    // boxShadow: [
    //   BoxShadow(
    //     color: Colors.black12,
    //     blurRadius: 5.0,
    //     offset: Offset(0, 2),
    //   ),
    // ],
  );
}
