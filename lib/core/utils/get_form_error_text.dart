import 'package:flutter/foundation.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

String getFormErrorText({@required FormBuilderState formState}) {
  String _errorText = '';

  for (int index = 0; index < formState.fields.length; index++) {
    String _fieldName = formState.fields.keys.toList()[index];
    if (formState.fields[_fieldName].hasError) {
      _errorText = '[ $_fieldName ] ${formState.fields[_fieldName].errorText}';
      break;
    }
  }
  return _errorText;
}
