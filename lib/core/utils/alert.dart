import 'dart:math';
import 'package:flash/flash.dart';
import 'package:flutter/material.dart';
import '../widgets/responsive.dart';
import 'constants.dart';

class Alert {
  void show({
    @required String message,
    Color messageColor = Colors.black,
    @required IconData alertIcon,
    Color alertIconColor = Colors.black,
    @required Color alertBackGroundColor,
    double alertHeight = 80.0,
    double alertBorderRadius = 5.0,
    FlashPosition alertPosition = FlashPosition.top,
    @required BuildContext context,
    Duration duration = const Duration(seconds: 5),
    flashStyle = FlashStyle.floating,
  }) {
    showFlash(
      context: context,
      duration: duration,
      builder: (context, controller) {
        return Flash(
          controller: controller,
          style: flashStyle,
          // boxShadows: kElevationToShadow[4],
          borderRadius: BorderRadius.circular(alertBorderRadius),
          backgroundColor: alertBackGroundColor,
          position: alertPosition,
          margin: EdgeInsets.all(5.0),
          horizontalDismissDirection: HorizontalDismissDirection.horizontal,
          child: Container(
            height: alertHeight,
            child: Stack(
              children: [
                Positioned(
                  top: -8,
                  left: -16,
                  child: ClipRRect(
                    child: Transform.rotate(
                      angle: 32 * pi / 180,
                      child: Icon(
                        alertIcon,
                        size: alertHeight * 1.5,
                        color: alertIconColor.withOpacity(0.1),
                      ),
                    ),
                  ),
                ),
                Center(
                  child: FlashBar(
                    message: Text(
                      message,
                      style: TextStyle(color: messageColor),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  void showAlertDialog({
    Widget title,
    @required Widget content,
    ShapeBorder dialogShape,
    EdgeInsetsGeometry contentPadding = const EdgeInsets.all(0.0),
    @required BuildContext context,
    bool actionsEnable = true,
    String cancelBtnLabel = 'Cancel',
    Color cancelLabelColor = Colors.white,
    Color cancelBtnColor = Colors.grey,
    Color cancelBtnSplashColor = Colors.white,
    BorderRadiusGeometry cancelBtnRadius,
    @required Function onCancel,
    String agreeBtnLabel = 'Agree',
    Color agreeBtnColor = Colors.grey,
    Color agreeBtnSplashColor = Colors.white,
    Color agreeLabelColor = Colors.white,
    @required Function onAgree,
    double buttonHeight = 48.0,
    EdgeInsetsGeometry buttonPadding = const EdgeInsets.all(10.0),
    BorderRadiusGeometry agreeBtnRadius,
    String buttonMode = "normal",
    //mode : "normal" , "no-space", "above", "single"
  }) {
    showDialog(
      context: context,
      builder: (_) {
        return Responsive(
            mobile: Dialog(
              title: title,
              content: content,
              dialogShape: dialogShape,
              contentPadding: contentPadding,
              actionsEnable: actionsEnable,
              onCancel: onCancel,
              cancelBtnLabel: cancelBtnLabel,
              cancelLabelColor: cancelLabelColor,
              cancelBtnColor: cancelBtnColor,
              cancelBtnSplashColor: cancelBtnSplashColor,
              cancelBtnRadius: cancelBtnRadius,
              onAgree: onAgree,
              agreeBtnLabel: agreeBtnLabel,
              agreeLabelColor: agreeLabelColor,
              agreeBtnColor: agreeBtnColor,
              agreeBtnSplashColor: agreeBtnSplashColor,
              agreeBtnRadius: agreeBtnRadius,
              buttonHeight: buttonHeight,
              buttonPadding: buttonPadding,
              buttonMode: buttonMode,
            ),
            tablet: Row(
              children: [
                Spacer(),
                Expanded(
                  flex: 4,
                  child: Dialog(
                    title: title,
                    content: content,
                    dialogShape: dialogShape,
                    contentPadding: contentPadding,
                    actionsEnable: actionsEnable,
                    onCancel: onCancel,
                    cancelBtnLabel: cancelBtnLabel,
                    cancelLabelColor: cancelLabelColor,
                    cancelBtnColor: cancelBtnColor,
                    cancelBtnSplashColor: cancelBtnSplashColor,
                    cancelBtnRadius: cancelBtnRadius,
                    onAgree: onAgree,
                    agreeBtnLabel: agreeBtnLabel,
                    agreeLabelColor: agreeLabelColor,
                    agreeBtnColor: agreeBtnColor,
                    agreeBtnSplashColor: agreeBtnSplashColor,
                    agreeBtnRadius: agreeBtnRadius,
                    buttonHeight: buttonHeight,
                    buttonPadding: buttonPadding,
                    buttonMode: buttonMode,
                  ),
                ),
                Spacer(),
              ],
            ),
            desktop: Row(
              children: [
                Spacer(),
                Expanded(
                  child: Dialog(
                      title: title,
                      content: content,
                      dialogShape: dialogShape,
                      contentPadding: contentPadding,
                      actionsEnable: actionsEnable,
                      onCancel: onCancel,
                      cancelBtnLabel: cancelBtnLabel,
                      cancelLabelColor: cancelLabelColor,
                      cancelBtnColor: cancelBtnColor,
                      cancelBtnSplashColor: cancelBtnSplashColor,
                      cancelBtnRadius: cancelBtnRadius,
                      onAgree: onAgree,
                      agreeBtnLabel: agreeBtnLabel,
                      agreeLabelColor: agreeLabelColor,
                      agreeBtnColor: agreeBtnColor,
                      agreeBtnSplashColor: agreeBtnSplashColor,
                      agreeBtnRadius: agreeBtnRadius,
                      buttonHeight: buttonHeight,
                      buttonPadding: buttonPadding,
                      buttonMode: buttonMode),
                ),
                Spacer()
              ],
            ));
      },
    );
  }
}

class Dialog extends StatelessWidget {
  final Widget title;
  final Widget content;
  final ShapeBorder dialogShape;
  final EdgeInsetsGeometry contentPadding;
  final bool actionsEnable;
  final Function onCancel;
  final String cancelBtnLabel;
  final Color cancelLabelColor;
  final Color cancelBtnColor;
  final Color cancelBtnSplashColor;
  final BorderRadiusGeometry cancelBtnRadius;
  final Function onAgree;
  final String agreeBtnLabel;
  final Color agreeLabelColor;
  final Color agreeBtnColor;
  final Color agreeBtnSplashColor;
  final BorderRadiusGeometry agreeBtnRadius;
  final double buttonHeight;
  final EdgeInsetsGeometry buttonPadding;
  final String buttonMode;
  const Dialog({
    this.title,
    @required this.content,
    this.dialogShape,
    this.contentPadding,
    this.actionsEnable,
    @required this.onCancel,
    this.cancelBtnLabel,
    this.cancelLabelColor,
    this.cancelBtnColor,
    this.cancelBtnSplashColor,
    this.cancelBtnRadius,
    @required this.onAgree,
    this.agreeBtnLabel,
    this.agreeLabelColor,
    this.agreeBtnColor,
    this.agreeBtnSplashColor,
    this.agreeBtnRadius,
    this.buttonHeight,
    this.buttonPadding,
    this.buttonMode,
  });
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: (title != null) ? title : null,
      contentPadding: contentPadding,
      content: content,
      shape: dialogShape,
      buttonPadding: (buttonMode == "no-space")
          ? const EdgeInsets.all(0.0)
          : buttonPadding,
      actions: actionsEnable
          ? [
              if (buttonMode == "normal" || buttonMode == "no-space")
                Container(
                  width: MediaQuery.of(context).size.width * 1,
                  child: Row(
                    children: [
                      Expanded(
                        child: Container(
                          height: buttonHeight,
                          child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                primary: cancelBtnColor,
                                onPrimary: cancelBtnSplashColor,
                                textStyle: TextStyle(color: Colors.white),
                                elevation: 0.0,
                                shadowColor: Colors.transparent,
                                shape: RoundedRectangleBorder(
                                  borderRadius: cancelBtnRadius ??
                                      BorderRadius.circular(5.0),
                                ),
                              ),
                              onPressed: onCancel,
                              child: Text(
                                cancelBtnLabel,
                                style: TextStyle(
                                  color: cancelLabelColor,
                                  fontFamily: CommonFonts.body,
                                ),
                              )),
                        ),
                      ),
                      if (buttonMode == "normal")
                        SizedBox(
                          width: 10.0,
                        ),
                      Expanded(
                        child: Container(
                          height: buttonHeight,
                          child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                primary: agreeBtnColor,
                                onPrimary: agreeBtnSplashColor,
                                textStyle: TextStyle(color: Colors.white),
                                elevation: 0.0,
                                shadowColor: Colors.transparent,
                                shape: RoundedRectangleBorder(
                                  borderRadius: agreeBtnRadius ??
                                      BorderRadius.circular(5.0),
                                ),
                              ),
                              onPressed: onAgree,
                              child: Text(
                                agreeBtnLabel,
                                style: TextStyle(
                                  color: agreeLabelColor,
                                  fontFamily: CommonFonts.body,
                                ),
                              )),
                        ),
                      ),
                    ],
                  ),
                ),
              if (buttonMode == "above")
                Container(
                  width: MediaQuery.of(context).size.width * 1,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Container(
                        height: buttonHeight,
                        child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              primary: agreeBtnColor,
                              onPrimary: agreeBtnSplashColor,
                              textStyle: TextStyle(color: Colors.white),
                              elevation: 0.0,
                              shadowColor: Colors.transparent,
                              shape: RoundedRectangleBorder(
                                borderRadius: agreeBtnRadius ??
                                    BorderRadius.circular(5.0),
                              ),
                            ),
                            onPressed: onAgree,
                            child: Text(
                              agreeBtnLabel,
                              style: TextStyle(
                                color: agreeLabelColor,
                                fontFamily: CommonFonts.body,
                              ),
                            )),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Container(
                        height: buttonHeight,
                        child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              primary: cancelBtnColor,
                              onPrimary: cancelBtnSplashColor,
                              textStyle: TextStyle(color: Colors.white),
                              elevation: 0.0,
                              shadowColor: Colors.transparent,
                              shape: RoundedRectangleBorder(
                                borderRadius: cancelBtnRadius ??
                                    BorderRadius.circular(5.0),
                              ),
                            ),
                            onPressed: onCancel,
                            child: Text(
                              cancelBtnLabel,
                              style: TextStyle(
                                color: cancelLabelColor,
                                fontFamily: CommonFonts.body,
                              ),
                            )),
                      ),
                    ],
                  ),
                ),
              if (buttonMode == "single")
                Container(
                  width: MediaQuery.of(context).size.width * 1,
                  child: Expanded(
                    child: Container(
                      height: buttonHeight,
                      child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: agreeBtnColor,
                            onPrimary: agreeBtnSplashColor,
                            textStyle: TextStyle(color: Colors.white),
                            elevation: 0.0,
                            shadowColor: Colors.transparent,
                            shape: RoundedRectangleBorder(
                              borderRadius:
                                  agreeBtnRadius ?? BorderRadius.circular(5.0),
                            ),
                          ),
                          onPressed: onAgree,
                          child: Text(
                            agreeBtnLabel,
                            style: TextStyle(
                              color: agreeLabelColor,
                              fontFamily: CommonFonts.body,
                            ),
                          )),
                    ),
                  ),
                )
            ]
          : [],
    );
  }
}
