import 'package:flutter/material.dart';
import '../utils/constants.dart';

InputDecoration fbTextFieldStyle({
  @required String labelText,
  @required ThemeData theme,
}) {
  return InputDecoration(
    contentPadding: EdgeInsets.only(left: 8.0),
    fillColor: theme.scaffoldBackgroundColor,
    filled: true,
    labelText: labelText,
    labelStyle: TextStyle(fontFamily: CommonFonts.body),
    enabledBorder: OutlineInputBorder(
      borderSide: BorderSide(
        color: theme.iconTheme.color,
      ),
    ),
    focusedBorder: OutlineInputBorder(
      borderSide: BorderSide(
        color: theme.primaryColor,
      ),
    ),
    focusedErrorBorder: OutlineInputBorder(
      borderSide: BorderSide(
        color: CommonColors.danger,
      ),
    ),
    errorBorder: OutlineInputBorder(
      borderSide: BorderSide(
        color: CommonColors.danger,
      ),
    ),
    errorStyle: TextStyle(color: CommonColors.danger),
    border: OutlineInputBorder(),
  );
}

InputDecoration fbSwitchStyle() {
  return InputDecoration(
    contentPadding: EdgeInsets.all(0.0),
    border: OutlineInputBorder(borderSide: BorderSide.none),
  );
}

InputDecoration fbLoginTextFieldStyle({
  @required String labelText,
  @required IconData icon,
  @required ThemeData theme,
  IconData suffixIcon,
  bool suffixIsExist = false,
  Function onPressedSuffixIcon,
}) {
  return InputDecoration(
    errorStyle:
        TextStyle(height: double.negativeInfinity, color: Colors.transparent),
    border: InputBorder.none,
    contentPadding: EdgeInsets.all(20.0),
    prefixIcon: Icon(
      icon,
      size: 28.0,
      color: theme.iconTheme.color,
    ),
    suffixIcon: (suffixIsExist)
        ? InkWell(
            onTap: onPressedSuffixIcon,
            child: Icon(
              suffixIcon,
            ),
          )
        : null,
    hintText: labelText,
    hintStyle: TextStyle(
      color: theme.iconTheme.color.withOpacity(0.70),
      fontFamily: CommonFonts.body,
      fontWeight: FontWeight.bold,
    ),
  );
}
