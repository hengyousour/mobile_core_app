import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:flutter/foundation.dart';

class FormatDateTime {
  String formatTimeStampToString(int timestamp) {
    var date = DateTime.fromMillisecondsSinceEpoch(timestamp);
    return DateFormat('dd/MM/yyyy').format(date);
  }

  Map<String, int> addDateTimeForMongoDB({
    @required DateTime dateTime,
    Duration duration,
  }) {
    return {'\$date': dateTime.add(duration).millisecondsSinceEpoch};
  }

  Map<String, int> subtractDateTimeForMongoDB({
    @required DateTime dateTime,
    Duration duration,
  }) {
    return {'\$date': dateTime.subtract(duration).millisecondsSinceEpoch};
  }

  Map<String, int> formatDateToDateTimeForMongoDB(
      {@required DateTime dateTime}) {
    DateTime _time = DateTime.now();
    DateTime _tempDateTime = DateTime(
        dateTime.year,
        dateTime.month,
        dateTime.day,
        _time.hour,
        _time.minute,
        _time.second,
        _time.millisecond,
        _time.microsecond);

    return {'\$date': _tempDateTime.millisecondsSinceEpoch};
  }
}
