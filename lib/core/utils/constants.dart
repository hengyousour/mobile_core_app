import 'package:flutter/material.dart';
import 'package:unicons/unicons.dart';
import 'dart:math';
import 'dart:ui';

class CommonColors {
  static const core = Color(0xFFFFFFFF);
  static const coreLight = Color(0xFFe8e9ed);
  static const primaryLight = Color(0xFFff8600);
  static const primary = Color(0xFFff8600);
  static const secondary = Color(0xFFa1ef7a);
  static const secondaryLight = Color(0xFFffab00);
  static const black = Color(0xFF000000);
  static const blackLight = Color(0xFF3a3941);
  static const success = Color(0xFF62c66e);
  static const successLight = Color(0xFF27c8a9);
  static const warning = Color(0xFFff8a5b);
  static const warningLight = Color(0xFFff9e39);
  static const danger = Color(0xFFfe4c52);
  static const dangerLight = Color(0xFFff835f);
  static const info = Color(0xFF249cd5);
  static const infoLight = Color(0xFF24d5c3);

  static List<Color> whiteGradients = [core, core];
  static List<Color> primaryGradients = [primary, secondary];
  static List<Color> successGradients = [success, successLight];
  static List<Color> warningGradients = [warning, warningLight];
  static List<Color> dangerGradients = [danger, dangerLight];
  static List<Color> infoGradients = [info, infoLight];

  //randomcolor
  static final Random _random = new Random();

  /// Returns a random color.
  static Color next() {
    return new Color(0xFF000000 + _random.nextInt(0x00FFFFFF));
  }
}

class CommonTextSize {
  static const titleTextSize = 60.0;
  static const headerTextSize = 36.0;
  static const subTitleTextSize = 24.0;
  static const bodyTextSizeWebAndDesktop = 16.0;
  static const bodyTextSizeTabletAndMobile = 14.0;
}

class CommonStyle {
  static const padding = 20.0;
  static const opacity = 0.35;
}

class CommonFonts {
  static const header = 'Ubuntu';
  static const body = 'Bahnschrift';
  static const kantumruy = 'Kantumruy';
  static const siemreap = 'Siemreap';
}

class CommonIcons {
  static const back = UniconsLine.angle_left_b;
  static const notification = UniconsLine.bell;
  static const ipAddress = UniconsLine.wifi_router;
  static const configIpAddress = UniconsLine.server_network;
  static const noConnection = UniconsLine.wifi_slash;
  static const edit = UniconsLine.pen;
  static const delete = UniconsLine.trash;
  static const alertSuccess = UniconsLine.grin;
  static const alertError = UniconsLine.meh;
  static const alertWaring = UniconsLine.exclamation_circle;
  static const alertInfo = UniconsLine.info_circle;
  static const camera = UniconsLine.camera;
  static const gallery = UniconsLine.image;
  static const menu = UniconsLine.subject;
  static const dashborad = UniconsLine.create_dashboard;
  static const user = UniconsLine.smile;
  static const password = UniconsLine.key_skeleton_alt;
  static const setting = UniconsLine.cog;
  static const qrCode = UniconsLine.qrcode_scan;
  static const logout = UniconsLine.left_arrow_from_left;
  static const darkMode = UniconsLine.moon;
  static const lightMode = UniconsLine.sun;
  static const hide = UniconsLine.eye_slash;
  static const show = UniconsLine.eye;
}

class CommonBoxShadow {
  final defualt = [
    BoxShadow(
      offset: Offset(0, 15),
      blurRadius: 32,
      color:
          CommonColors.black.withOpacity(0.12), // Black color with 12% opacity
    ),
  ];

  final glowing = [
    BoxShadow(
      color: CommonColors.secondary.withOpacity(0.1),
      spreadRadius: 1,
      blurRadius: 16,
      offset: Offset(8, 0),
    ),
    BoxShadow(
      color: CommonColors.primary.withOpacity(0.2),
      spreadRadius: 6,
      blurRadius: 16,
      offset: Offset(-8, 0),
    ),
    BoxShadow(
      color: CommonColors.secondary.withOpacity(0.2),
      spreadRadius: 6,
      blurRadius: 16,
      offset: Offset(8, 0),
    ),
  ];
}
