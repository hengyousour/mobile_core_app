import 'package:flutter/foundation.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import '../utils/alert.dart';
import '../utils/constants.dart';
import '../models/setting/setting_model.dart';
import '../storages/connection_storage.dart';
import '../storages/user_storage.dart';

class SettingProvider with ChangeNotifier {
  String _ipAddress;
  String get ipAddress => _ipAddress;
  String _language;
  String get language => _language;

  void getSettingDocFromLocalStorage() async {
    _ipAddress = await ConnectionStorage().getIpAddress();
    _language = await UserStorage().getLanguage();
    notifyListeners();
  }

  void insertSetting({
    @required SettingModel formDoc,
    @required BuildContext context,
  }) async {
    if (formDoc.language == "km") {
      context.locale = Locale('km', 'KH');
    }

    if (formDoc.language == "en") {
      context.locale = Locale('en', 'US');
    }

    UserStorage().setLanguage(lang: formDoc.language);
    Alert().show(
      message: 'Good job, your setting has been successfully changed.',
      alertIcon: CommonIcons.alertSuccess,
      alertBackGroundColor: CommonColors.success,
      context: context,
    );
  }
}
