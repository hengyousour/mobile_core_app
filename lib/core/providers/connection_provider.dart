import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import '../../app.dart';

class ConnectionProvider with ChangeNotifier {
  bool _isConnected;
  bool get isConnected => _isConnected;
  void realTimeConnection() {
    meteor.status().listen((event) {
      _isConnected = event.connected;
      notifyListeners();
    });
  }
}
