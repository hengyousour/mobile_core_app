import 'package:flutter/foundation.dart';
import 'package:image_picker/image_picker.dart';
import '../storages/user_storage.dart';

class ProfileProvider with ChangeNotifier {
  String _imagePath;
  String get getImagePath => _imagePath;
  Map<String, dynamic> _userDoc;
  Map<String, dynamic> get userDoc => _userDoc;

  void getUserDocFromLocalStorage() async {
    _imagePath = await UserStorage().getUserImagePath();
    _userDoc = await UserStorage().getUser();
    notifyListeners();
  }

  void setUserImagePath({@required ImageSource source}) async {
    final picker = ImagePicker();
    final pickedFile = await picker.getImage(source: source);

    if (pickedFile != null) {
      UserStorage().setUserImagePath(_imagePath);
    } else {
      print('No image selected.');
    }
  }
}
