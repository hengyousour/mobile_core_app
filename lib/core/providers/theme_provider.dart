import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import '../utils/constants.dart';

class ThemeProvider with ChangeNotifier {
  ThemeMode themeMode = ThemeMode.light;

  bool get isDarkmode => themeMode == ThemeMode.dark;

  void toggleTheme(bool isOn) {
    themeMode = isOn ? ThemeMode.dark : ThemeMode.light;
    notifyListeners();
  }
}

class AppTheme {
  static final darkTheme = ThemeData(
    primaryColor: CommonColors.success,
    accentColor: CommonColors.successLight,
    scaffoldBackgroundColor: CommonColors.blackLight,
    colorScheme: ColorScheme.dark(primary: CommonColors.success),
    iconTheme: IconThemeData(color: CommonColors.core),
    primaryIconTheme: IconThemeData(color: CommonColors.success),
    textSelectionTheme: TextSelectionThemeData(
      cursorColor: CommonColors.success,
    ),
    fontFamily: CommonFonts.body,
  );

  static final lightTheme = ThemeData(
    primaryColor: CommonColors.primary,
    accentColor: CommonColors.secondary,
    scaffoldBackgroundColor: CommonColors.core,
    colorScheme: ColorScheme.light(primary: CommonColors.primary),
    iconTheme: IconThemeData(color: CommonColors.blackLight),
    primaryIconTheme: IconThemeData(color: CommonColors.primary),
    textSelectionTheme: TextSelectionThemeData(
      cursorColor: CommonColors.primary,
    ),
    fontFamily: CommonFonts.body,
  );
}
