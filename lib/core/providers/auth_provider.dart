// import 'package:dart_meteor/dart_meteor.dart';
import 'package:dart_meteor_web/dart_meteor_web.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/foundation.dart';
import '../utils/alert.dart';
import '../utils/constants.dart';
import '../models/auth/login_model.dart';
import '../storages/auth_storage.dart';
import '../storages/user_storage.dart';
import '../../app.dart';

class AuthProvider with ChangeNotifier {
  bool _loading = false;
  bool get loading => _loading;
  bool _obsurceText = true;
  bool get obsurceText => _obsurceText;

  void logIn({
    @required LogInModel formDoc,
    @required BuildContext context,
  }) {
    _loading = true;
    meteor
        .loginWithPassword(formDoc.username, formDoc.password,
            delayOnLoginErrorSecond: 2)
        .then((result) {
      _loading = false;
      AuthStorage().setUserLoginToken(userLoginToken: result);
      Navigator.of(context).pushReplacementNamed('/home');
      UserStorage().setUser(userDoc: meteor.userCurrentValue());
      UserStorage().setLanguage(lang: "en");
      notifyListeners();
    }).catchError((err) {
      if (err is MeteorError) {
        Alert().show(
            message: err.message,
            alertIcon: CommonIcons.alertError,
            alertBackGroundColor: CommonColors.danger,
            context: context);
        _loading = false;
        notifyListeners();
      }
    });
    notifyListeners();
  }

  void toggleSuffixIcon() {
    _obsurceText = !_obsurceText;
    notifyListeners();
  }
}
