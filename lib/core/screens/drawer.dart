import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:easy_localization/easy_localization.dart';
import '../providers/menu_provider.dart';
import '../providers/profile_porvider.dart';
import '../providers/theme_provider.dart';
import '../storages/auth_storage.dart';
import '../storages/user_storage.dart';
import '../utils/constants.dart';
import '../../app.dart';

class Menu extends StatefulWidget {
  final List<MenuItem> mainMenu;
  final Function(int) callback;
  final int current;

  Menu(
    this.mainMenu, {
    Key key,
    this.callback,
    this.current,
  });

  @override
  _MenuState createState() => _MenuState();
}

class _MenuState extends State<Menu> {
  final widthBox = SizedBox(
    width: 16.0,
  );

  @override
  Widget build(BuildContext context) {
    final _theme = Theme.of(context);
    final TextStyle androidStyle = TextStyle(
        fontSize: 14,
        fontWeight: FontWeight.bold,
        color: _theme.colorScheme.background);
    final TextStyle iosStyle = TextStyle(
      color: _theme.colorScheme.background,
    );
    final style = kIsWeb
        ? androidStyle
        : Platform.isAndroid
            ? androidStyle
            : iosStyle;

    return Scaffold(
      body: Container(
        color: _theme.primaryColor,
        // decoration: BoxDecoration(
        //   gradient: LinearGradient(
        //     colors: [
        //       _theme.primaryColor,
        //       _theme.accentColor,
        //     ],
        //     begin: Alignment.topLeft,
        //     end: Alignment.bottomRight,
        //   ),
        // ),
        child: SafeArea(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Spacer(),
              Padding(
                padding: const EdgeInsets.only(left: 15.0),
                child: Consumer<ProfileProvider>(
                  builder: (_, state, child) {
                    return CircleAvatar(
                      backgroundColor: Colors.white,
                      backgroundImage: state.getImagePath != null
                          ? FileImage(File(state.getImagePath))
                          : AssetImage("assets/images/logo.png"),
                      radius: 26.0,
                    );
                  },
                ),
              ),
              SizedBox(height: 8.0),
              Padding(
                padding: const EdgeInsets.only(left: 15.0),
                child: Consumer<ProfileProvider>(
                  builder: (_, state, child) => Text(
                    'Hello,\n${state.userDoc != null ? state.userDoc['username'] : 'unknown'}'
                        .toUpperCase(),
                    style: TextStyle(color: _theme.colorScheme.background),
                  ),
                ),
              ),
              SizedBox(height: 20.0),
              Selector<MenuProvider, int>(
                selector: (_, provider) => provider.currentPage,
                builder: (_, index, __) => Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 15.0),
                      child: Consumer<ThemeProvider>(
                        builder: (_, state, child) => Row(children: [
                          state.isDarkmode
                              ? Icon(
                                  CommonIcons.darkMode,
                                  size: 28.0,
                                  color: _theme.colorScheme.background,
                                )
                              : Icon(
                                  CommonIcons.lightMode,
                                  size: 28.0,
                                  color: _theme.colorScheme.background,
                                ),
                          Switch.adaptive(
                              value: state.isDarkmode,
                              onChanged: (value) {
                                state.toggleTheme(value);
                              }),
                        ]),
                      ),
                    ),
                    ...widget.mainMenu
                        .map((item) => MenuItemWidget(
                              key: Key(item.index.toString()),
                              item: item,
                              callback: widget.callback,
                              widthBox: widthBox,
                              style: style,
                              selected: index == item.index,
                            ))
                        .toList()
                  ],
                ),
              ),
              Spacer(),
              Padding(
                padding: const EdgeInsets.only(left: 24.0, right: 24.0),
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      elevation: 0.0,
                      shape: RoundedRectangleBorder(
                        side: BorderSide(
                          color: _theme.colorScheme.background,
                          width: 2.0,
                        ),
                        borderRadius: BorderRadius.circular(16.0),
                      ),
                      textStyle:
                          TextStyle(color: _theme.colorScheme.background),
                      shadowColor: Colors.transparent),
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Text(
                      tr("logout").toUpperCase(),
                      style: TextStyle(fontSize: 18),
                    ),
                  ),
                  onPressed: () {
                    AuthStorage().clearUserLoginToken();
                    UserStorage().clearUser();
                    context.read<MenuProvider>().clearCurrentPage();
                    context.read<ThemeProvider>().toggleTheme(false);
                    meteor.logout();
                    Navigator.of(context).pushReplacementNamed('/app');
                  },
                ),
              ),
              Spacer(),
            ],
          ),
        ),
      ),
    );
  }
}

class MenuItemWidget extends StatelessWidget {
  final MenuItem item;
  final Widget widthBox;
  final TextStyle style;
  final Function callback;
  final bool selected;

  final white = Colors.white;

  const MenuItemWidget(
      {Key key,
      this.item,
      this.widthBox,
      this.style,
      this.callback,
      this.selected})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
      style: TextButton.styleFrom(
        backgroundColor: selected
            ? CommonColors.blackLight.withOpacity(CommonStyle.opacity)
            : null,
        padding: EdgeInsets.all(20.0),
        shape: RoundedRectangleBorder(),
      ),
      onPressed: () => callback(item.index),

      // color:
      child: Row(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Icon(
            item.icon,
            color: style.color,
            size: 24,
          ),
          widthBox,
          Expanded(
            child: Text(
              item.title.toUpperCase(),
              style: style,
            ),
          )
        ],
      ),
    );
  }
}

class MenuItem {
  final String title;
  final IconData icon;
  final int index;

  const MenuItem(this.title, this.icon, this.index);
}
