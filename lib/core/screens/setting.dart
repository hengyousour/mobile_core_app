import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:provider/provider.dart';
import 'package:easy_localization/easy_localization.dart';
import '../models/setting/setting_model.dart';
import '../providers/setting_provider.dart';
import '../services/select_options/static_options.dart';
import '../utils/constants.dart';
import '../utils/custom_form_build_style.dart';

class Setting extends StatelessWidget {
  static final GlobalKey<FormBuilderState> _fbKey =
      GlobalKey<FormBuilderState>();

  @override
  Widget build(BuildContext context) {
    final _theme = Theme.of(context);
    context.read<SettingProvider>().getSettingDocFromLocalStorage();
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.all(10.0),
            child: FormBuilder(
              key: _fbKey,
              child: Column(
                children: [
                  Consumer<SettingProvider>(
                    builder: (_, state, child) => FormBuilderTextField(
                      enabled: false,
                      name: 'ipAddress',
                      initialValue: state.ipAddress,
                      style: TextStyle(fontFamily: CommonFonts.body),
                      decoration: fbTextFieldStyle(
                        labelText: "setting.ipAddress".tr(),
                        theme: _theme,
                      ),
                      validator: FormBuilderValidators.compose(
                          [FormBuilderValidators.required(context)]),
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Consumer<SettingProvider>(
                    builder: (_, state, child) => FormBuilderDropdown(
                      items: StaticOptions()
                          .languageOpts
                          .map((options) => DropdownMenuItem(
                              value: options.value, child: Text(options.label)))
                          .toList(),
                      decoration: fbTextFieldStyle(
                        labelText: "Language",
                        theme: _theme,
                      ),
                      name: "language",
                      initialValue: state.language,
                      validator: FormBuilderValidators.compose(
                          [FormBuilderValidators.required(context)]),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        bottomNavigationBar: Container(
          child: Row(
            children: [
              Expanded(
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    textStyle: TextStyle(color: Colors.white),
                    padding: EdgeInsets.all(20.0),
                    elevation: 0.0,
                    shadowColor: Colors.transparent,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(0.0),
                    ),
                  ),
                  onPressed: () async {
                    if (_fbKey.currentState.saveAndValidate()) {
                      final SettingModel _formDoc =
                          SettingModel.fromJson(_fbKey.currentState.value);
                      Provider.of<SettingProvider>(context, listen: false)
                          .insertSetting(formDoc: _formDoc, context: context);
                    }
                  },
                  child: Text('form.save'.tr(),
                      style: TextStyle(
                          fontSize: 20, fontFamily: CommonFonts.body)),
                ),
              ),
              Expanded(
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: CommonColors.blackLight,
                    textStyle: TextStyle(color: Colors.white),
                    padding: EdgeInsets.all(20.0),
                    elevation: 0.0,
                    shadowColor: Colors.transparent,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(0.0),
                    ),
                  ),
                  onPressed: () {
                    _fbKey.currentState.reset();
                  },
                  child: Text('form.reset'.tr(),
                      style: TextStyle(
                          fontSize: 20, fontFamily: CommonFonts.body)),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
