import 'package:flutter/material.dart';
import '../../core/widgets/connection/connection_form.dart';
import '../../core/widgets/responsive.dart';

//ignore: must_be_immutable
class Connecting extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final ThemeData _theme = Theme.of(context);
    return Scaffold(
      body: Container(
        // color: _theme.primaryColor,
        decoration: BoxDecoration(color: _theme.primaryColor),
        child: Center(
          child: SingleChildScrollView(
            child: Responsive(
              desktop: Row(children: [
                Spacer(),
                Expanded(
                  child: ConnectionForm(theme: _theme),
                ),
                Spacer(),
              ]),
              tablet: Row(
                children: [
                  Spacer(),
                  Expanded(flex: 2, child: ConnectionForm(theme: _theme)),
                  Spacer(),
                ],
              ),
              mobile: ConnectionForm(theme: _theme),
            ),
          ),
        ),
      ),
    );
  }
}
