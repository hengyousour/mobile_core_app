import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:provider/provider.dart';
import '../providers/profile_porvider.dart';
import '../utils/constants.dart';
import '../../core/widgets/dashboard/dashboard_grid.dart';

class Dashboard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _theme = Theme.of(context);
    context.read<ProfileProvider>().getUserDocFromLocalStorage();

    return Container(
      height: double.infinity,
      width: double.infinity,
      // decoration: BoxDecoration(
      //   gradient: LinearGradient(
      //     begin: Alignment.topCenter,
      //     end: Alignment.bottomCenter,
      //     colors: [
      //       _theme.primaryColor,
      //       _theme.accentColor,
      //     ],
      //   ),
      // ),
      child: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: [
                      Text(
                        'dashboard.hello'.tr(),
                        style: TextStyle(
                          color: _theme.iconTheme.color,
                          fontSize: 20.0,
                          fontFamily: CommonFonts.header,
                        ),
                      ),
                      Consumer<ProfileProvider>(
                        builder: (_, state, child) => Text(
                          '${state.userDoc != null ? state.userDoc['username'] : 'unknown'}',
                          style: TextStyle(
                            color: _theme.iconTheme.color,
                            fontSize: 20.0,
                            fontFamily: CommonFonts.header,
                          ),
                        ),
                      )
                    ],
                  ),
                ],
              ),
              SizedBox(
                height: 10.0,
              ),
              Expanded(
                child: DashBoardGrid(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
