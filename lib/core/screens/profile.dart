import 'dart:io';
import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import '../providers/profile_porvider.dart';
import '../utils/constants.dart';

class Profile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _theme = Theme.of(context);
    return Container(
      height: double.infinity,
      width: double.infinity,
      // decoration: BoxDecoration(
      //   gradient: LinearGradient(
      //     begin: Alignment.topCenter,
      //     end: Alignment.bottomCenter,
      //     colors: [
      //       _theme.primaryColor,
      //       _theme.accentColor,
      //     ],
      //   ),
      // ),
      child: SafeArea(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Container(
            child: MediaQuery.of(context).orientation == Orientation.portrait
                ? _profileHeaderPortrait(theme: _theme, context: context)
                : _profileHeaderLandscape(theme: _theme, context: context),
          ),
        ],
      )),
    );
  }

  Column _profileHeaderPortrait(
      {@required ThemeData theme, @required BuildContext context}) {
    return Column(
      children: <Widget>[
        BounceInDown(
          child: Stack(children: [
            Consumer<ProfileProvider>(
              builder: (_, state, chlid) => Container(
                width: 120.0,
                height: 120.0,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(
                    color: CommonColors.core,
                    width: 3.0,
                  ),
                  image: DecorationImage(
                      fit: BoxFit.cover,
                      image: (state.getImagePath != null)
                          ? FileImage(File(state.getImagePath))
                          : AssetImage('assets/images/logo.png')),
                ),
              ),
            ),
            Positioned(
                bottom: 1,
                right: 1,
                child: InkWell(
                  onTap: () {
                    _settingModalBottomSheet(context);
                  },
                  child: Container(
                    height: 40.0,
                    width: 40.0,
                    decoration: BoxDecoration(
                      color: theme.primaryColor,
                      shape: BoxShape.circle,
                      border: Border.all(width: 3.0, color: CommonColors.core),
                    ),
                    child: Icon(
                      CommonIcons.edit,
                      color: CommonColors.core,
                    ),
                  ),
                ))
          ]),
        ),
        ZoomIn(
          child: Consumer<ProfileProvider>(
            builder: (_, state, child) => Text(
              state.userDoc['username'] ?? 'unknown'.toUpperCase(),
              style: TextStyle(
                  color: theme.iconTheme.color,
                  fontWeight: FontWeight.bold,
                  fontSize: 40.0,
                  fontFamily: CommonFonts.header),
            ),
          ),
        ),
        ZoomIn(
          child: Consumer<ProfileProvider>(
            builder: (_, state, child) => Text(
                state.userDoc['email'] ?? 'unknown@gmail.com',
                style: TextStyle(
                    color: theme.iconTheme.color,
                    fontSize: 16.0,
                    fontFamily: CommonFonts.body)),
          ),
        ),
      ],
    );
  }

  Row _profileHeaderLandscape(
      {@required ThemeData theme, @required BuildContext context}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        BounceInDown(
          child: Stack(children: [
            Consumer<ProfileProvider>(
              builder: (_, state, child) => Container(
                width: 120.0,
                height: 120.0,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(
                    color: CommonColors.core,
                    width: 3.0,
                  ),
                  image: DecorationImage(
                      fit: BoxFit.cover,
                      image: (state.getImagePath != null)
                          ? FileImage(File(state.getImagePath))
                          : AssetImage('assets/images/logo.png')),
                ),
              ),
            ),
            Positioned(
                bottom: 1,
                right: 1,
                child: InkWell(
                  onTap: () {
                    _settingModalBottomSheet(context);
                  },
                  child: Container(
                    height: 40.0,
                    width: 40.0,
                    decoration: BoxDecoration(
                      color: theme.primaryColor,
                      shape: BoxShape.circle,
                      border: Border.all(width: 3.0, color: CommonColors.core),
                    ),
                    child: Icon(
                      CommonIcons.edit,
                      color: CommonColors.core,
                    ),
                  ),
                ))
          ]),
        ),
        SizedBox(
          width: 10,
        ),
        Column(
          children: [
            ZoomIn(
              child: Consumer<ProfileProvider>(
                builder: (_, state, child) => Text(
                  state.userDoc['username'] ?? 'unknown'.toUpperCase(),
                  style: TextStyle(
                      color: theme.iconTheme.color,
                      fontWeight: FontWeight.bold,
                      fontSize: 40.0,
                      fontFamily: CommonFonts.header),
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            ZoomIn(
              child: Consumer<ProfileProvider>(
                builder: (_, state, child) => Text(
                    state.userDoc['email'] ?? 'unknown@gmail.com',
                    style: TextStyle(
                        color: theme.iconTheme.color,
                        fontSize: 16.0,
                        fontFamily: CommonFonts.body)),
              ),
            ),
          ],
        ),
      ],
    );
  }

  void _settingModalBottomSheet(BuildContext context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: Wrap(
              children: <Widget>[
                ListTile(
                  leading: Icon(CommonIcons.camera),
                  title: Text(
                    'Camera',
                    style: TextStyle(fontFamily: CommonFonts.body),
                  ),
                  onTap: () {
                    Provider.of<ProfileProvider>(context, listen: false)
                        .setUserImagePath(source: ImageSource.camera);
                  },
                ),
                ListTile(
                  leading: Icon(CommonIcons.gallery),
                  title: Text(
                    'Gallery',
                    style: TextStyle(fontFamily: CommonFonts.body),
                  ),
                  onTap: () {
                    Provider.of<ProfileProvider>(context, listen: false)
                        .setUserImagePath(source: ImageSource.gallery);
                  },
                ),
              ],
            ),
          );
        });
  }
}
