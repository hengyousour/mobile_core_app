import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';
import 'package:animate_do/animate_do.dart';
import '../../core/screens/real_time_connection.dart';
import '../../core/providers/connection_provider.dart';
import '../widgets/responsive.dart';
import '../providers/auth_provider.dart';
import '../utils/constants.dart';
import '../widgets/login/login_form.dart';

class Login extends StatelessWidget {
  static final GlobalKey<FormBuilderState> _fbKey =
      GlobalKey<FormBuilderState>();
  final int _year = DateTime.now().year;

  @override
  Widget build(BuildContext context) {
    context.read<ConnectionProvider>().realTimeConnection();
    bool _isConenction = context
        .select<ConnectionProvider, bool>((provider) => provider.isConnected);
    final ThemeData _theme = Theme.of(context);

    return Scaffold(
        body: GestureDetector(
      onTap: () {
        WidgetsBinding.instance.focusManager.primaryFocus?.unfocus();
      },
      child: Stack(children: [
        Container(
          height: double.infinity,
          width: double.infinity,
          child: Center(
            child: SingleChildScrollView(
              child: FadeIn(
                duration: Duration(milliseconds: 1800),
                child: Column(
                  children: <Widget>[
                    Text(
                      'RABBIT',
                      style: TextStyle(
                        color: _theme.iconTheme.color,
                        fontSize: CommonTextSize.titleTextSize,
                        letterSpacing: 1.5,
                        fontWeight: FontWeight.bold,
                        fontFamily: CommonFonts.header,
                      ),
                    ),
                    Text(
                      'welcome back, have a good day !'.toUpperCase(),
                      style: TextStyle(
                        color: _theme.iconTheme.color,
                        fontFamily: CommonFonts.header,
                      ),
                    ),
                    SizedBox(
                      height: 20.0,
                    ),
                    Consumer<AuthProvider>(
                      builder: (_, state, child) => Visibility(
                        visible: state.loading,
                        child: SpinKitThreeBounce(
                          color: _theme.primaryColor,
                          size: 20.0,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    Responsive(
                      desktop: Row(
                        children: [
                          Spacer(),
                          Expanded(
                            child: LoginForm(fbKey: _fbKey, theme: _theme),
                          ),
                          Spacer(),
                        ],
                      ),
                      tablet: Row(
                        children: [
                          Spacer(),
                          Expanded(
                              flex: 6,
                              child: LoginForm(fbKey: _fbKey, theme: _theme)),
                          Spacer(),
                        ],
                      ),
                      mobile: LoginForm(fbKey: _fbKey, theme: _theme),
                    ),
                    Text(
                      'Copyright \u00a9 $_year Rabbit Technology . All Right Reserved.',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: _theme.iconTheme.color,
                          fontFamily: CommonFonts.body,
                          fontSize: 14.0),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
        if (_isConenction == false) RealTimeConnection()
      ]),
    ));
  }
}
