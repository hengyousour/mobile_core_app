import 'dart:math' show pi;
import 'package:mobile_core_app/core/providers/connection_provider.dart';
import 'package:mobile_core_app/core/screens/real_time_connection.dart';
import 'home.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:flutter_zoom_drawer/flutter_zoom_drawer.dart';
import 'package:provider/provider.dart';
import '../utils/constants.dart';
import '../providers/menu_provider.dart';
import '../screens/dashboard.dart';
import '../screens/profile.dart';
import '../screens/setting.dart';

class PageStructure extends StatelessWidget {
  final String title;
  final Widget child;
  final List<Widget> actions;
  final Color backgroundColor;
  final double elevation;

  const PageStructure({
    Key key,
    this.title,
    this.child,
    this.actions,
    this.backgroundColor,
    this.elevation,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    context.read<ConnectionProvider>().realTimeConnection();
    bool _isConenction = context
        .select<ConnectionProvider, bool>((provider) => provider.isConnected);
    final angle = ZoomDrawer.isRTL() ? 180 * pi / 180 : 0.0;
    final _currentPage =
        context.select<MenuProvider, int>((provider) => provider.currentPage);
    Widget _page;

    switch (_currentPage) {
      case 0:
        _page = Dashboard();
        break;
      case 1:
        _page = Profile();
        break;
      case 2:
        _page = Setting();
        break;
      default:
        _page = Dashboard();
    }

    final _theme = Theme.of(context);
    return PlatformScaffold(
        // backgroundColor: Colors.transparent,
        material: (_, __) => MaterialScaffoldData(
              extendBodyBehindAppBar: true,
              resizeToAvoidBottomInset: false,
            ),
        appBar: PlatformAppBar(
          automaticallyImplyLeading: false,
          material: (_, __) => MaterialAppBarData(
            elevation: elevation,
            centerTitle: true,
            backgroundColor: Colors.transparent,
          ),
          title: PlatformText(
            Home.mainMenu[_currentPage].title,
            style: TextStyle(
              color: _theme.iconTheme.color,
              fontFamily: CommonFonts.header,
            ),
          ),
          leading: Transform.rotate(
            angle: angle,
            child: PlatformIconButton(
              icon: Icon(
                CommonIcons.menu,
                color: _theme.iconTheme.color,
              ),
              onPressed: () {
                ZoomDrawer.of(context).toggle();
              },
            ),
          ),
          trailingActions: actions,
        ),
        // bottomNavBar:
        //         PlatformNavBar(
        //             currentIndex: _currentPage,
        //             itemChanged: (index) =>
        //                 Provider.of<MenuProvider>(context, listen: false)
        //                     .updateCurrentPage(index),
        //             items: HomeScreen.mainMenu
        //                 .map(
        //                   (item) => BottomNavigationBarItem(
        //                     title: Text(
        //                       item.title,
        //                       style: style,
        //                     ),
        //                     icon: Icon(
        //                       item.icon,
        //                       color: color,
        //                     ),
        //                   ),
        //                 )
        //                 .toList(),
        //           )
        // ,
        body: Stack(
          children: [
            _page,
            if (_isConenction == false) RealTimeConnection(),
          ],
        ));
  }
}
