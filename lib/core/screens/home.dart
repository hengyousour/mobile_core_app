import 'package:badges/badges.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_zoom_drawer/flutter_zoom_drawer.dart';
import 'package:provider/provider.dart';
import 'drawer.dart';
import 'page_structure.dart';
import '../providers/menu_provider.dart';
import '../providers/theme_provider.dart';
import '../utils/constants.dart';
import '../../route_generator.dart';

class Home extends StatefulWidget {
  static List<MenuItem> mainMenu = [
    MenuItem("dashboard", CommonIcons.dashborad, 0),
    MenuItem("profile", CommonIcons.user, 1),
    MenuItem("setting", CommonIcons.setting, 2),
  ];

  @override
  _HomeState createState() => new _HomeState();
}

class _HomeState extends State<Home> {
  final _drawerController = ZoomDrawerController();

  int _currentPage = 0;

  @override
  Widget build(BuildContext context) {
    final themeProvider = Provider.of<ThemeProvider>(context);
    return PlatformApp(
      material: (_, __) => MaterialAppData(
        debugShowCheckedModeBanner: false,
        themeMode: themeProvider.themeMode,
        theme: AppTheme.lightTheme,
        darkTheme: AppTheme.darkTheme,
      ),
      onGenerateRoute: RouteGenerator.generateRoute,
      home: ZoomDrawer(
        controller: _drawerController,
        menuScreen: Menu(
          Home.mainMenu,
          callback: _updatePage,
          current: _currentPage,
        ),
        mainScreen: MainScreen(),
        borderRadius: 24.0,
//      showShadow: true,
        angle: 0.0,
        slideWidth: MediaQuery.of(context).size.width *
            (ZoomDrawer.isRTL() ? .45 : 0.65),
        // openCurve: Curves.fastOutSlowIn,
        // closeCurve: Curves.bounceIn,
      ),
    );
  }

  void _updatePage(index) {
    Provider.of<MenuProvider>(context, listen: false).updateCurrentPage(index);
    _drawerController.toggle();
  }
}

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  @override
  Widget build(BuildContext context) {
    final _theme = Theme.of(context);
    final rtl = ZoomDrawer.isRTL();
    return ValueListenableBuilder<DrawerState>(
      valueListenable: ZoomDrawer.of(context).stateNotifier,
      builder: (context, state, child) {
        return AbsorbPointer(
          absorbing: state != DrawerState.closed,
          child: child,
        );
      },
      child: GestureDetector(
        child: PageStructure(
          elevation: 0.0,
          actions: [
            Padding(
              padding: const EdgeInsets.only(right: 20.0),
              child: Badge(
                badgeColor: CommonColors.danger,
                position: BadgePosition(top: 5, end: -10),
                elevation: 0,
                badgeContent: Text(
                  '0',
                  style: TextStyle(color: _theme.colorScheme.background),
                ),
                child: Icon(
                  CommonIcons.notification,
                  color: _theme.iconTheme.color,
                ),
              ),
            )
          ],
        ),
        onPanUpdate: (details) {
          if (details.delta.dx < 6 && !rtl || details.delta.dx < -6 && rtl) {
            ZoomDrawer.of(context).toggle();
          }
        },
      ),
    );
  }
}
