import 'package:flutter/material.dart';
import '../../core/utils/constants.dart';

class RealTimeConnection extends StatelessWidget {
  final String text;
  final Color textColor;
  final Color backgroundColor;
  const RealTimeConnection(
      {Key key,
      this.text = 'Oops , No Internet Connection',
      this.textColor = CommonColors.core,
      this.backgroundColor = CommonColors.danger})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Container(
        width: double.infinity,
        padding: EdgeInsets.all(20.0),
        color: backgroundColor,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(CommonIcons.alertWaring, color: textColor),
            SizedBox(
              width: 10.0,
            ),
            Text(
              text,
              style: TextStyle(
                color: textColor,
                fontFamily: CommonFonts.body,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
