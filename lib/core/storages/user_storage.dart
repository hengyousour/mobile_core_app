import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/foundation.dart';

class UserStorage {
  // Create SharedPreferences
  Future<SharedPreferences> _userPrefs = SharedPreferences.getInstance();

  void setUser({@required Map<String, dynamic> userDoc}) async {
    SharedPreferences userPrefs = await _userPrefs;
    userPrefs.setString('id', userDoc['_id']);
    userPrefs.setString('username', userDoc['username']);
    userPrefs.setString('email', userDoc['emails'][0]['address']);
  }

  Future<Map<String, dynamic>> getUser() async {
    SharedPreferences userPrefs = await _userPrefs;
    Map<String, dynamic> _user = {
      'id': userPrefs.getString('id'),
      'username': userPrefs.getString('username'),
      'email': userPrefs.getString('email'),
    };

    return _user;
  }

  setUserImagePath(String path) async {
    SharedPreferences userPrefs = await _userPrefs;
    if (path != null) {
      userPrefs.setString('imagePath', path);
    }
  }

  Future<String> getUserImagePath() async {
    SharedPreferences userPrefs = await _userPrefs;
    return userPrefs.getString('imagePath');
  }

  setLanguage({String lang}) async {
    SharedPreferences userPrefs = await _userPrefs;
    if (lang != null) {
      await userPrefs.setString('language', lang);
    }
  }

  Future<String> getLanguage() async {
    SharedPreferences userPrefs = await _userPrefs;
    return userPrefs.getString('language');
  }

  void clearUser() async {
    SharedPreferences userPrefs = await _userPrefs;
    userPrefs.remove('_id');
    userPrefs.remove('username');
    userPrefs.remove('email');
    userPrefs.remove('imagePath');
    userPrefs.remove('language');
  }
}
