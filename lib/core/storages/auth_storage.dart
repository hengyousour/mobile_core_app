import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/foundation.dart';

class AuthStorage {
  // Create SharedPreferences
  Future<SharedPreferences> _authPrefs = SharedPreferences.getInstance();

  void setUserLoginToken({@required userLoginToken}) async {
    SharedPreferences authPrefs = await _authPrefs;
    authPrefs.setString('loginToken', userLoginToken.token);
    // authPrefs.setString('loginTokenExpires',
    //     userLoginToken.tokenExpires.millisecondsSinceEpoch.toString());
  }

  Future<DateTime> getUserLoginTokenExpire() async {
    SharedPreferences authPrefs = await _authPrefs;
    DateTime _expires;

    int millisecond = int.parse(authPrefs.getString('loginTokenExpires'));
    _expires = DateTime.fromMillisecondsSinceEpoch(millisecond);

    return _expires;
  }

  Future<String> getUserLoginToken() async {
    SharedPreferences authPrefs = await _authPrefs;
    return authPrefs.getString('loginToken');
  }

  Future<bool> isUserLoginTokenExist() async {
    SharedPreferences authPrefs = await _authPrefs;
    return authPrefs.getString('loginToken') != null ? true : false;
  }

  void clearUserLoginToken() async {
    SharedPreferences authPrefs = await _authPrefs;
    authPrefs.remove('loginToken');
  }
}
