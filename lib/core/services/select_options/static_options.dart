import '../../models/select-option/select_option_model.dart';

class StaticOptions {
  List<SelectOptionModel> _languageOpts = [
    SelectOptionModel(label: "KH", value: "km"),
    SelectOptionModel(label: "US", value: "en"),
  ];

  List<SelectOptionModel> get languageOpts {
    return [..._languageOpts];
  }
}
