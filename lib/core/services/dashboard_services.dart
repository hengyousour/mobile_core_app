import 'package:flutter/widgets.dart';
import '../models/dashboard/dashboard_model.dart';
import '../utils/constants.dart';

class DashboardServices {
  final BuildContext context;
  DashboardServices({@required this.context});

  List<DashboardModel> _dashboardList = [
    DashboardModel(
      index: 0,
      title: "Item 1",
      iconName: CommonIcons.gallery,
      onPressed: (context) {},
      bgColors: [
        CommonColors.core.withOpacity(0.10),
        CommonColors.core.withOpacity(0.10)
      ],
    ),
    DashboardModel(
      index: 1,
      title: "Item 2",
      iconName: CommonIcons.gallery,
      onPressed: (context) {},
      bgColors: [
        CommonColors.core.withOpacity(0.15),
        CommonColors.core.withOpacity(0.15)
      ],
    ),
    DashboardModel(
      index: 2,
      title: "Item 3",
      iconName: CommonIcons.gallery,
      onPressed: (context) {},
      bgColors: [
        CommonColors.core.withOpacity(0.15),
        CommonColors.core.withOpacity(0.15)
      ],
    ),
    DashboardModel(
      index: 3,
      title: "Item 4",
      iconName: CommonIcons.gallery,
      onPressed: (context) {},
      bgColors: [
        CommonColors.core.withOpacity(0.15),
        CommonColors.core.withOpacity(0.15)
      ],
    ),
  ];

  List<DashboardModel> get dashboardList => [..._dashboardList];
}
