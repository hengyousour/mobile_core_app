import '../../app.dart';

class LogInServices {
  Map<String, dynamic> _branchDoc;
  Future<Map<String, dynamic>> getBranchDoc({String branchId}) async {
    _branchDoc = await meteor.call('microfis_findOneBranch', [
      {'selector': branchId}
    ]);
    return _branchDoc;
  }
}
