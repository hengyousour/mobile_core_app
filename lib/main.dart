import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:provider/provider.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'app.dart';
import 'core/providers/auth_provider.dart';
import 'core/providers/profile_porvider.dart';
import 'core/providers/setting_provider.dart';
import 'core/providers/theme_provider.dart';
import 'core/providers/menu_provider.dart';
import 'core/providers/connection_provider.dart';

void main() async {
  runApp(
    EasyLocalization(
      supportedLocales: [Locale('km', 'KH'), Locale('en', 'US')],
      path: 'assets/translations',
      fallbackLocale: Locale('en', 'US'),
      child: MultiProvider(
        providers: [
          ChangeNotifierProvider(
            create: (ctx) => ThemeProvider(),
          ),
          ChangeNotifierProvider(
            create: (ctx) => ProfileProvider(),
          ),
          ChangeNotifierProvider(
            create: (ctx) => SettingProvider(),
          ),
          ChangeNotifierProvider(
            create: (ctx) => MenuProvider(),
          ),
          ChangeNotifierProvider(
            create: (ctx) => AuthProvider(),
          ),
          ChangeNotifierProvider(
            create: (ctx) => ConnectionProvider(),
          ),
        ],
        child: Phoenix(
          child: App(),
        ),
      ),
    ),
  );
}
