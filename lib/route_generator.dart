import 'package:flutter/material.dart';
import 'app.dart';
import 'core/screens/home.dart';
import 'core/screens/profile.dart';
import 'core/screens/login.dart';
import 'core/screens/setting.dart';
import 'core/screens/dashboard.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    // final args = settings.arguments;
    switch (settings.name) {
      case '/app':
        return MaterialPageRoute(builder: (_) => App());
        break;
      case '/home':
        return MaterialPageRoute(builder: (_) => Home());
        break;
      case '/login':
        return MaterialPageRoute(builder: (_) => Login());
        break;
      case '/dashboard':
        return MaterialPageRoute(builder: (_) => Dashboard());
        break;
      case '/profile':
        return MaterialPageRoute(builder: (_) => Profile());
        break;
      case '/setting':
        return MaterialPageRoute(builder: (_) => Setting());
        break;
      default:
        return _errorRoute();
        break;
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(title: Text('Error')),
        body: Center(child: Text('Error')),
      );
    });
  }
}
