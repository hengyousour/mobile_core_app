import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
// import 'package:dart_meteor/dart_meteor.dart';
import 'package:dart_meteor_web/dart_meteor_web.dart';
import 'route_generator.dart';
import 'core/providers/theme_provider.dart';
import 'core/screens/home.dart';
import 'core/screens/connecting.dart';
import 'core/screens/login.dart';
import 'core/storages/connection_storage.dart';
import 'core/storages/auth_storage.dart';

MeteorClient meteor = MeteorClient.connect(url: '192.168.1.11:3000');

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  bool _userLoginTokenExist;
  String _ipAddress;
  @override
  void didChangeDependencies() async {
    super.didChangeDependencies();

    // Set dynamic ip address
    _ipAddress = await ConnectionStorage().getIpAddress();
    if (_ipAddress != null) {
      setState(() {
        meteor = MeteorClient.connect(url: 'http://$_ipAddress');
      });
    }

    //isUserLoginTokenExist
    _userLoginTokenExist = await AuthStorage().isUserLoginTokenExist();
  }

  @override
  Widget build(BuildContext context) {
    final _themeProvider = Provider.of<ThemeProvider>(context, listen: false);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      themeMode: _themeProvider.themeMode,
      theme: AppTheme.lightTheme,
      darkTheme: AppTheme.darkTheme,
      home: StreamBuilder<DdpConnectionStatus>(
        stream: meteor.status(),
        builder: (context, snapshot) {
          //checking connection
          if (snapshot.hasData && snapshot.data.connected) {
            //checking user token if exist go to landing page
            if (_userLoginTokenExist) {
              return Home();
            } else {
              return Login();
            }
          } else if (_ipAddress != null && snapshot.data.connected) {
            if (_userLoginTokenExist) {
              return Home();
            } else {
              return Login();
            }
          } else {
            return Connecting();
          }
        },
      ),
      onGenerateRoute: RouteGenerator.generateRoute,
    );
  }
}
